import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

ipin = 17
GPIO.setup(ipin,GPIO.OUT)
print("LED green ON")
GPIO.output(ipin,GPIO.HIGH)
time.sleep(1)
print("LED green OFF")
GPIO.output(ipin,GPIO.LOW)
time.sleep(1)

ipin = 20
GPIO.setup(ipin,GPIO.OUT)
print("LED blue ON")
GPIO.output(ipin,GPIO.HIGH)
time.sleep(1)
print("LED blue OFF")
GPIO.output(ipin,GPIO.LOW)
time.sleep(1)

ipin = 16
GPIO.setup(ipin,GPIO.OUT)
print("LED yellow ON")
GPIO.output(ipin,GPIO.HIGH)
time.sleep(1)
print("LED yellow OFF")
GPIO.output(ipin,GPIO.LOW)
time.sleep(1)

ipin = 12
GPIO.setup(ipin,GPIO.OUT)
print("LED red ON")
GPIO.output(ipin,GPIO.HIGH)
time.sleep(1)
print("LED red OFF")
GPIO.output(ipin,GPIO.LOW)
time.sleep(1)


