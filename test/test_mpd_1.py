from mpd import MPDClient

client = MPDClient()
client.timeout = 10                # network timeout in seconds (floats allowed), default: None
client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
client.connect("localhost", 6602)
print(client.mpd_version)

print(client.find("any", "playlist")) # print result of the command "find any house"
print(client.status())

client.update()
playlist_dir__1 = "playlist_1"
client.add(playlist_dir__1)

print(client.stats())


print(client.currentsong())


client.close()                     # send the close command
client.disconnect()

