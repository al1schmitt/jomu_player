#!/usr/bin/env python
import os
import sys
import argparse
#str_autostartup = 'sudo systemctl start mpd.service'
str_autostartup = 'sudo mpd &'


def StartFromCommandLine( argv ) :	
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)    
    parser.add_argument("-r", "--reset", dest="reset", action='store_true',
                    help="reset switch. if this reset flag is true, autostartup is removed from rc.local")
    options = parser.parse_args()
           
    str_found = False
    with open('/etc/rc.local') as fin:
        for line in fin:
            if str_autostartup in line:
                str_found = True
                print("INFO: autostartup line found.")
                break
    if str_found and not options.reset:
        print("INFO: autostartup line already found in rc.local. exiting.")
        exit()
    elif str_found and options.reset:
        print("INFO: autostartup line to be removed now.")
        with open('/etc/rc.local') as fin:
            with open('/etc/rc.local.TMP', "w") as fout:
                for line in fin:
                    if str_autostartup in line:    
                        print("INFO: autostartup line successfully removed from rc.local.")
                    else:
                        fout.write(line)
        # save original version (just in case)
        os.rename('/etc/rc.local', '/etc/rc.local.jic')
        print("INFO: rc.local saved as rc.local.jic")
        os.rename('/etc/rc.local.TMP', '/etc/rc.local')
    elif not options.reset:
        print("INFO: autostartup line to be added now.")
        with open('/etc/rc.local') as fin:
            with open('/etc/rc.local.TMP', "w") as fout:
                for line in fin:
                    #print("line is <{}>".format(line))
                    if 'exit 0' in line :
                        fout.write('{}\n'.format(str_autostartup))
                        print("INFO: autostartup line successfully added to rc.local.")
                    fout.write(line)
        # save original version (just in case)
        os.rename('/etc/rc.local', '/etc/rc.local.jic')
        print("INFO: rc.local saved as rc.local.jic")
        os.rename('/etc/rc.local.TMP', '/etc/rc.local')

    
def Main( argv ) :	
    try:
        StartFromCommandLine( argv )
    except (KeyboardInterrupt):
        exit()

 
if __name__ == '__main__':
    print("You are using setup_autostartup.py")
    Main( sys.argv )
