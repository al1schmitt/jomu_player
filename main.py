#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 14:41:43 2019

@author: aschmitt
"""
from mpd import MPDClient
import os
import time
import logging
from datetime import datetime
import RPi.GPIO as GPIO

def get_datestr():
    return datetime.now().strftime('%Y%m%d_%H-%M-%S')


# logging setup
logPath="/home/aschmitt/Documents/perso/gitlab/jomu_player/logs"
if not os.path.isdir(logPath):
    os.makedirs(logPath)
logfileName="log_jomu_{}".format(get_datestr())
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
    handlers=[
        logging.FileHandler("{0}/{1}.log".format(logPath, logfileName)),
        logging.StreamHandler()
    ])
logger = logging.getLogger()

def do_fadeout():
    
    logger.info("fadeout")
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)    

    sleep_time = 0.1
    for a in range(g_volume):
        client.setvol(g_volume-a)
        sleep_time = sleep_time/2
        time.sleep(sleep_time)
    
    client.pause()
        
    client.setvol(g_volume)
    
    client.close()
    client.disconnect()
    
        

def init():
    do_set_led_green(False)
    do_set_led_blue(False)
    do_set_led_yellow(False)
    
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)
    logger.info("version = {}".format(client.mpd_version))
    
    client.setvol(g_volume)
    
    logger.debug("update = {}".format(client.update()))
    logger.debug("clear = {}".format(client.clear()))
    
    playlist_dir_intro = "playlist_boot"
    client.add(playlist_dir_intro)
    
    client.update()
    client.play()
    time.sleep(10)
    client.stop()
    
    client.close()
    client.disconnect()
    do_set_led_green(True)

def ctrl_play_list(index):
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)
    
    logger.debug("update = {}".format(client.update()))
    logger.debug("clear = {}".format(client.clear()))
    
    playlist_dir = "playlist_{}".format(index)
    client.add(playlist_dir)
    
    client.update()
    client.play()
    
    client.close()
    client.disconnect()  

def ctrl_next():
    do_fadeout()
    
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)
        
    client.next()
    
    client.close()
    client.disconnect()  
    
def ctrl_stop():
    do_fadeout()
    
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)
    
    client.stop()
    
    client.close()
    client.disconnect()   
    
def ctrl_play():
    
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)
    
    client.play()
    
    client.close()
    client.disconnect() 
    
    
def ctrl_startstop():
    client = MPDClient()
    client.timeout = 10                # network timeout in seconds (floats allowed), default: None
    client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
    client.connect("localhost", 6602)
    state = client.status()['state']
    logger.info("state = {}".format(state))
    client.close()
    client.disconnect()  
    
    if state=="stop" or state=="pause":
        ctrl_play()
    else:
        do_fadeout()
      
def do_blink_all_playlists_leds(secs_init_wait):
    n_leds = 5
    loop_begin_wait = 1
    unit_wait = 0.15 #int( (float(secs_init_wait)-n_leds)/n_leds )
    n_times_blink = int( (float(secs_init_wait))/(n_leds*2.*unit_wait+loop_begin_wait) )
    
    for a in range(n_times_blink):
        time.sleep(loop_begin_wait) 
        time.sleep(unit_wait)  
        do_set_led_red(1)    
        time.sleep(unit_wait)  
        do_set_led_red(0)
        
        time.sleep(unit_wait)  
        do_set_led_green_playlist(1)    
        time.sleep(unit_wait)  
        do_set_led_green_playlist(0)
        
        time.sleep(unit_wait)  
        do_set_led_blue(1)    
        time.sleep(unit_wait)  
        do_set_led_blue(0)
    
        time.sleep(unit_wait)  
        do_set_led_yellow(1)    
        time.sleep(unit_wait)  
        do_set_led_yellow(0)
        
        time.sleep(unit_wait)  
        do_set_led_green_playlist2(1)    
        time.sleep(unit_wait)  
        do_set_led_green_playlist2(0)
 
        
def do_set_led_green(state):
    global led_green_pin 
    if state:
        GPIO.output(led_green_pin,GPIO.HIGH)
    else:
        GPIO.output(led_green_pin,GPIO.LOW)

def do_set_led_red(state):
    global led_red_pin 
    if state:
        GPIO.output(led_red_pin,GPIO.HIGH)
    else:
        GPIO.output(led_red_pin,GPIO.LOW)
    
def do_set_led_green_playlist(state):
    global led_green_playlist_pin 
    if state:
        GPIO.output(led_green_playlist_pin,GPIO.HIGH)
    else:
        GPIO.output(led_green_playlist_pin,GPIO.LOW)

def do_set_led_green_playlist2(state):
    global led_green_playlist2_pin 
    if state:
        GPIO.output(led_green_playlist2_pin,GPIO.HIGH)
    else:
        GPIO.output(led_green_playlist2_pin,GPIO.LOW)

def do_set_led_blue(state):
    global led_blue_pin 
    if state:
        GPIO.output(led_blue_pin,GPIO.HIGH)
    else:
        GPIO.output(led_blue_pin,GPIO.LOW)

def do_set_led_yellow(state):
    global led_yellow_pin 
    if state:
        GPIO.output(led_yellow_pin,GPIO.HIGH)
    else:
        GPIO.output(led_yellow_pin,GPIO.LOW)
        
def do_set_allplaylistleds(state):
    global led_blue_pin, led_yellow_pin 
    if state:
        GPIO.output(led_blue_pin,GPIO.HIGH)
        GPIO.output(led_yellow_pin,GPIO.HIGH)
        GPIO.output(led_red_pin,GPIO.HIGH)
    else:
        GPIO.output(led_blue_pin,GPIO.LOW)
        GPIO.output(led_yellow_pin,GPIO.LOW)
        GPIO.output(led_red_pin,GPIO.LOW)

def callback_p1(channel):
    global g_enabletrig
    if GPIO.input(channel):
        logger.info("callback: push Detected P1! ch={}".format(channel))
        if g_enabletrig:
            #g_enabletrig = 0
            do_set_allplaylistleds(False)
            do_set_led_blue(True)
            ctrl_play_list(1)
        else:
            logger.info("callback: inhibited".format())
    else:
        #print(" Movement Detected!")
        pass
    
def callback_p2(channel):
    global g_enabletrig
    if GPIO.input(channel):
        logger.info("callback: push Detected P2! ch={}".format(channel))
        if g_enabletrig:
            #g_enabletrig = 0
            do_set_allplaylistleds(False)
            do_set_led_yellow(True)
            ctrl_play_list(2)
        else:
            logger.info("callback: inhibited".format())
    else:
        #print(" Movement Detected!")
        pass
    
def callback_p3(channel):
    global g_enabletrig
    if GPIO.input(channel):
        logger.info("callback: push Detected p3! ch={}".format(channel))
        if g_enabletrig:
            #g_enabletrig = 0
            do_set_allplaylistleds(False)
            do_set_led_red(True)
            ctrl_play_list(3)
        else:
            logger.info("callback: inhibited".format())
    else:
        #print(" Movement Detected!")
        pass
    
def callback_startstop(channel):
    global g_enabletrig
    if GPIO.input(channel):
        logger.info("callback: push Detected STARTSTOP! ch={}".format(channel))
        if g_enabletrig:
            #g_enabletrig = 0
            ctrl_startstop()
        else:
            logger.info("callback: inhibited".format())
    else:
        #print(" Movement Detected!")
        pass

# GPIO SETUP
channel_p1 = 14
GPIO.setmode(GPIO.BCM)
#GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
#GPIO.setup(channel, GPIO.IN)
GPIO.setup(channel_p1, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(channel_p1, GPIO.RISING, bouncetime=300)  # let us know when the pin goes HIGH or LOW
GPIO.add_event_callback(channel_p1, callback_p1)  # assign function to GPIO PIN, Run function on change

channel_p2 = 15
GPIO.setmode(GPIO.BCM)
#GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
#GPIO.setup(channel, GPIO.IN)
GPIO.setup(channel_p2, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(channel_p2, GPIO.RISING, bouncetime=300)  # let us know when the pin goes HIGH or LOW
GPIO.add_event_callback(channel_p2, callback_p2)  # assign function to GPIO PIN, Run function on change

channel_p3 = 7
GPIO.setmode(GPIO.BCM)
#GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
#GPIO.setup(channel, GPIO.IN)
GPIO.setup(channel_p3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(channel_p3, GPIO.FALLING, bouncetime=300)  # let us know when the pin goes HIGH or LOW
GPIO.add_event_callback(channel_p3, callback_p3)  # assign function to GPIO PIN, Run function on change

channel_startstop = 18
GPIO.setmode(GPIO.BCM)
#GPIO.setmode(GPIO.BOARD) # Use physical pin numbering
#GPIO.setup(channel, GPIO.IN)
GPIO.setup(channel_startstop, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(channel_startstop, GPIO.RISING, bouncetime=300)  # let us know when the pin goes HIGH or LOW
GPIO.add_event_callback(channel_startstop, callback_startstop)  # assign function to GPIO PIN, Run function on change

GPIO.setwarnings(False)
led_green_pin = 17
GPIO.setup(led_green_pin, GPIO.OUT)
led_blue_pin = 20
GPIO.setup(led_blue_pin, GPIO.OUT)
led_yellow_pin = 16
GPIO.setup(led_yellow_pin, GPIO.OUT)
led_red_pin = 12
GPIO.setup(led_red_pin, GPIO.OUT)
led_green_playlist_pin = 19
GPIO.setup(led_green_playlist_pin, GPIO.OUT)
led_green_playlist2_pin = 13
GPIO.setup(led_green_playlist2_pin, GPIO.OUT)

# global variables
g_enabletrig = 1 

# init 
g_volume = 20


if 0:
    #ctrl_stop()
    ctrl_play()
    print("NOW EXITING")
    exit()

ctrl_stop()
secs_init_wait = 20
logger.info("now waiting/blinking {} sec".format(secs_init_wait))
#time.sleep(secs_init_wait)
do_blink_all_playlists_leds(secs_init_wait=secs_init_wait)
init()
time.sleep(1)
logger.info("init done")

if 0:
    ctrl_play_list(1)
    time.sleep(100)
    ctrl_next()
    time.sleep(600)
    ctrl_stop()


# infinite loop
while True:
    time.sleep(1)
