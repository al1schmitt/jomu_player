#sudo pip3 install python-mpd2
from mpd import MPDClient
import time

client = MPDClient()
client.timeout = 10                # network timeout in seconds (floats allowed), default: None
client.idletimeout = None          # timeout for fetching the result of the idle command is handled seperately, default: None
client.connect("localhost", 6602)
print("INFO: version = {}".format(client.mpd_version))

client.update()

print("INFO: find = {}".format(client.find("any", "playlist")))

print("INFO: listplaylists = {}".format(client.listplaylists()))
#print("INFO: playlistclear = {}".format(client.playlistclear()))

for a in range(1):
    print("INFO: clear = {}".format(client.clear()))
#    print("INFO: delete = {}".format(client.delete()))


print("\n")
print("INFO: status = {}".format(client.status()))

client.iterate = True
for song in client.playlistinfo():
    print(song["file"])

playlist_dir = "playlist_3"
client.add(playlist_dir)
#playlist_dir_1 = "playlist_1"
#client.add(playlist_dir_1)

client.update()

print("\n")
print("INFO: stats = {}".format(client.stats()))

print("\n")
print("INFO: current song = {}".format(client.currentsong()))

n_repeat = 1
for a in range(n_repeat):
    print("INFO: play")
    client.play()
    time.sleep(2.1)
    print("INFO: pause")
    client.pause()
    
    time.sleep(1.)
    print("INFO: next")
    client.next()

print("INFO: next")
client.stop()

client.close()                     # send the close command
client.disconnect()

