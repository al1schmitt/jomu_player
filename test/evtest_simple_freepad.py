#import evdev
#sudo pip3 install evdev
from evdev import InputDevice, categorize, ecodes

#creates object 'gamepad' to store the data
#you can call it whatever you like
gamepad = InputDevice('/dev/input/event20')

#prints out device info at start
print(gamepad)

#print(ecodes)

previous_event = ""
previous2_event = ""
previous3_event = ""
previous4_event = ""
previous5_event = ""
#evdev takes care of polling the controller in a loop
for event in gamepad.read_loop():
    if True:#event.type == ecodes.EV_KEY:
        #print(categorize(event))
        ev = event
        if 1:
            print("DETAIL: ev = {}".format(ev))
            
        #playlist 1 (thumb d 1)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 288, type 01, val 00" in previous_event):
                if ("code 04, type 04, val 589825" in previous2_event):
                    if ("code 00, type 00, val 00" in previous3_event):
                        if ("code 288, type 01, val 01" in previous4_event):
                            if ("code 04, type 04, val 589825" in previous5_event):
                                print("\n")
                                print("INFO: playlist 1")
        
        #playlist 2 (thumb d 2)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 289, type 01, val 00" in previous_event):
                if ("code 04, type 04, val 589826" in previous2_event):
                    if ("code 00, type 00, val 00" in previous3_event):
                        if ("code 289, type 01, val 01" in previous4_event):
                            if ("code 04, type 04, val 589826" in previous5_event):
                                print("\n")
                                print("INFO: playlist 2")
                                
        #playlist 3 (thumb d 3)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 290, type 01, val 00" in previous_event):
                if ("code 04, type 04, val 589827" in previous2_event):
                    if ("code 00, type 00, val 00" in previous3_event):
                        if ("code 290, type 01, val 01" in previous4_event):
                            if ("code 04, type 04, val 589827" in previous5_event):
                                print("\n")
                                print("INFO: playlist 3")
                                
        #playlist 4 (thumb d 4)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 291, type 01, val 00" in previous_event):
                if ("code 04, type 04, val 589828" in previous2_event):
                    if ("code 00, type 00, val 00" in previous3_event):
                        if ("code 291, type 01, val 01" in previous4_event):
                            if ("code 04, type 04, val 589828" in previous5_event):
                                print("\n")
                                print("INFO: playlist 4")
                                
        
        #up unused
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 01, type 03, val 127" in previous_event):
                if ("code 00, type 00, val 00" in previous2_event):
                    if ("code 01, type 03, val 00" in previous3_event):
                        if ("code 00, type 00, val 00" in previous4_event):
                            print("\n")
                            print("INFO: up unused")
                            
        #next  (thumb g right)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 16, type 03, val 00" in previous_event):
                if ("code 00, type 00, val 00" in previous2_event):
                    if ("code 16, type 03, val 01" in previous3_event):
                        print("\n")
                        print("INFO: next")
                        
        #previous  (thumb g left)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 16, type 03, val 00" in previous_event):
                if ("code 00, type 00, val 00" in previous2_event):
                    if ("code 16, type 03, val -1" in previous3_event):
                        print("\n")
                        print("INFO: previous")
                        
        #stop (molette droite appuyée)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 299, type 01, val 00" in previous_event):
                if ("code 04, type 04, val 589836" in previous2_event):
                    if ("code 00, type 00, val 00" in previous3_event):
                        if ("code 299, type 01, val 01" in previous4_event):
                            if ("code 04, type 04, val 589836" in previous5_event):
                                print("\n")
                                print("INFO: stop D")
                        
        #stop (molette gauche appuyée)
        if ("code 00, type 00, val 00" in str(ev)):
            if ("code 298, type 01, val 00" in previous_event):
                if ("code 04, type 04, val 589835" in previous2_event):
                    if ("code 00, type 00, val 00" in previous3_event):
                        if ("code 298, type 01, val 01" in previous4_event):
                            if ("code 04, type 04, val 589835" in previous5_event):
                                print("\n")
                                print("INFO: stop G")
                                
        
        previous5_event = previous4_event
        previous4_event = previous3_event
        previous3_event = previous2_event
        previous2_event = previous_event
        previous_event = str(event)
            